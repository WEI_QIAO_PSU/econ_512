function fval = question5(p_guess,Tolerance,imax)
p0 = [0.5;0.5;0.5];
p_new = ones(3,1);
for i = 1:imax
   y0=FOC(p0);
   y1=FOC(p_guess);
% For each j, we take a single secant step
	for j = 1:3;
        result = (p0(j)*y1(j)-p_guess(j)*y0(j))/(y1(j)-y0(j));
		p_new(j,1)=result;
    end
			  if max(abs((p_new-p_guess)))<Tolerance
			     fval(1:3) = p_new;
			     fval(4) = i;
			     break;
			  end
    p0 = p_guess;
    p_guess = p_new;
end
if i == imax
   fprintf('Exceeding the max iteration value and solution not found')
   fval(1:3) = inf;
   fval(4) = imax;
end
