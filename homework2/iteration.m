% This function excuate the iteration method for
function fval = iteration(p,Tolerance,imax)
  % This is the function of solving the root for FOC using a particular iteration rule
  % Input
  % FUN is the name of a real valued function, need to be a string
  % p is the initial point for iteration
  % Tolerance defines the tolerance value of stopping
  % imax is the maximal iteration value
  % Output
  % fval(1) is the root, fval(2) is the iteration number

  for i = 1:imax
    p1 = 1./(1-demand(p));
    if max(abs((p1-p)))<Tolerance
      fval(1:3) = p1;
      fval(4) = i;
      break;
    end
    p = p1;
  end
  if i == imax
    fprintf('Exceeding the max iteration value and solution not found')
    fval(1:3) = inf;
    fval(4) = imax;
  end
  
