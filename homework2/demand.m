% This is the demand function for homework 2 empirical method
% Input is a vector of price
% Output is a vector of demand
% To be compatible with CEtools, we define the output as a row vector


function y = demand(p)
 n = length(p);
 v = -1.*ones(n,1);
 q = zeros(n,1);
 denominator = 1+sum(exp(v-p));
 for i = 1:n;
  q(i) = exp(v(i)-p(i))/denominator;
 end
 y = q;
end
