% This is the first order derivative for problem 2, homework 2 empirical method
% Input is a vector of price
% Output is the value of first order derivative, we seek to find the root for this function using Broyden method
% To be compatible with CEtools, we define the output as a row vector

function v = FOC(p)
  q = demand(p);
% To avoid the root [0, 0 ,0], we here divide both sides by q. The first order candition can be rewritten as the following
  v = 1+p.*q-p;
end
