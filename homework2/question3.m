function fval = question3(p_guess,Tolerance,imax)
p0 = [0.5;0.5;0.5];
p_new = ones(3,1);
for i = 1:imax
	for j = 1:3;
		result=secant('FOC',p0,p_guess,j,Tolerance,imax);
		p_new(j,1)=result(1);
   end
			  if max(abs((p_new-p_guess)))<Tolerance
			     fval(1:3) = p_new;
			     fval(4) = i;
			     break;
			  end
    po = p_guess;
    p_guess = p_new;
end
if i == imax
   fprintf('Exceeding the max iteration value and solution not found')
   fval(1:3) = inf;
   fval(4) = imax;
end
