function fval = secant(Fun,x0,x1,j,Tolerance,imax)
  % This is the function of solving the root for real valued functions using scent method
  % Input
  % FUN is the name of a real valued function, need to be a string
  % x0 and x1 are two initial points for secant method
  % Tolerance defines the tolerance value of stopping
  % imax is the maximal iteration value
  % Output
  % fval(1) is the root, fval(2) is the iteration number

  for i = 1:imax
    y1=feval(Fun,x1);
    y0=feval(Fun,x0);
    x_new = (x0(j)*y1(j)-y0(j)*x1(j))/(y1(j)-y0(j));
    if abs((x_new-x1(j))/x1(j))<Tolerance
      fval(1) = x_new;
      fval(2) = i;
      break;
    end
    x0(j) = x1(j);
    x1(j) = x_new;
  end
  if i == imax
    fprintf('Exceeding the max iteration value and solution not found')
    fval(1) = inf;
    fval(2) = imax;
  end
