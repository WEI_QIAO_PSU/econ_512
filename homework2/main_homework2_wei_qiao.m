% Homework2 for empirical method class
% Wei Qiao by Matlab 2014R
% This .m file will require the demand.m, FOC.m, secant.m question3.m, iteration.m question5.m and CETools
% user need to add CETools to their working directory

% use section separator double percent sign please.
%% Problem 1
disp('problem1');
p=[1;1;1];
disp('Demand is:');
demand(p)

%% Problem 2
disp('problem2');
p_guess = [1,0,0,3;
           1,0,1,2;
           1,0,2,1;];
for i=1:4
           tic
           disp(i);
           disp('Bertrand Equilibrium is:');
           broyden('FOC',p_guess(:,i))
           toc
           disp(toc);
end

%% Problem 3
disp('problem3');
imax = 1000;
Tolerance = 10^(-8);

for i=1:4
           tic
           disp(i);
           disp('Bertrand Equilibrium is:');
           question3(p_guess(:,i),Tolerance,imax)
           toc
           disp(toc);
end

%% Problem 4
disp('problem4');
for i=1:4
           tic
           disp(i);
           disp('Bertrand Equilibrium is:');
           iteration(p_guess(:,i),10^(-8),1000)
           toc
           disp(toc);
end

%% Problem 5
disp('problem5');
for i=1:4
           tic
           disp(i);
           disp('Bertrand Equilibrium is:');
           question5(p_guess(:,i),10^(-8),10000)
           toc
           disp(toc);
end 
