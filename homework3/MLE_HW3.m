% This is the MLE estimation of part A, homework 3 empirical method
% Input is a a value of theta_1
% theta_2 will be recovered using theta_2=theta_1/Y1
% Output is the value of first order derivative of the log likelihood function, we seek to find the root for this function using Newton's method

function theta = MLE_HW3(Y)

  fun = @(theta) FOC_HW3(theta,Y);
  result = newton(fun,1);
  % you were not supposed to use generic optimization tech
  theta = result(1);
  if flag >=1;
    disp('MLE estimator not found');
    theta = inf;
  end
end
