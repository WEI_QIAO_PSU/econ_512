
% This is the MLE estimation by BHHH algorithm, for part B, homework 3 empirical method
% Input 1 is vector of coeficient need to be estimated, we require beta being a column vector
% Tolerance defines the tolerance value of stopping
% imax is the maximal iteration value

% Output is the estimated beta

function [v,iter,J] = BHHH_mle(beta_0,Tolerance,imax,X,y)

   for i = 1:imax
% instead of using loop as in lhd.m, to sppedup, we use a matrix operation to calculate hessian and gradient
       hessian = (((y-exp(X*beta_0))*ones(1,6).*X)'*((y-exp(X*beta_0))*ones(1,6).*X))^(-1);
       gradient = (sum((y-exp(X*beta_0))*ones(1,6).*X))';
% updating the estimation for beta according to BHHH algorithm
       beta_new = beta_0 + hessian*gradient;

   if max(abs((beta_new-beta_0)))<Tolerance
      v = beta_new;
      iter = i
      J = hessian;
      break;
   end
   beta_0 = beta_new;
   end
   if i == imax
   fprintf('Exceeding the max iteration value and solution not found')
   v = inf;
   iter = imax;
   J = zeros(6,6);
   end
end
