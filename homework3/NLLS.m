% This is the squared residual function given a set of paprameter value beta, for part B, homework 3 empirical method
% Input is vector of coeficient need to be estimated, we require beta being a column vector
% Output is the value of log likelihood function, we seek to find the root for this function using various optimization method

function [fval, fjac] = NLLS(beta,X,y)
   fval = sum((y-exp(X*beta)).^2);
   intermediate = zeros(6,1);
       for i = 1:601;
       intermediate = intermediate + 2*exp(X(i,1:6)*beta)*(-X(i,1:6))'*(y(i)-exp(X(i,1:6)*beta));
       end
   fjac = intermediate;
end
