% This is the log-likelihood function given a set of paprameter value beta, for part B, homework 3 empirical method
% Input is vector of coeficient need to be estimated, we require beta being a column vector
% Output is the value of log likelihood function, we seek to find the root for this function using various optimization method
function v = lhd2(beta,X,y)
  v = (-sum(exp(X*beta)) + sum(y.*(X*beta)) - sum(log(factorial(y))));
end
