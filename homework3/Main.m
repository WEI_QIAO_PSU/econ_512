%% Part A
% user need to define the value for Y1 and Y2, the following is an example
Y1 = 1.1;
Y2 = 1;
Y = Y1/Y2;

disp('theta 1 is')
theta_1 = MLE_HW3(Y)
disp('theta 2 is')
theta_2 = theta_1/Y1
disp('covariance matrix is')
[-trigamma(theta_1), 1/theta_2;
  1/theta_2,         -1/(theta_2^2)]^(-1)

% function plot

step = (3-1.1)/100;
y = 1.1:step:3;
thetaa = zeros(1,101);
for i = 1:101
 thetaa(i) = MLE_HW3(y(i));
end
plot(y,thetaa)

% Part B

%% problem 1 MLE estimation
load('hw3.mat')
beta_0 = zeros(6,1);
fun = @(beta) lhd(beta,X,y);
disp('MLE Estimation using quasi-newton method without providing gradient');
[x,fval,exitflag,output] = fminunc(fun,beta_0)


% Note that SpecifyObjectiveGradient is only an option of FMINUNC for matlab latter than 2016
disp('MLE Estimation using quasi-newton method with gradient');
options = optimoptions('fminunc','SpecifyObjectiveGradient',true);
[x,fval,exitflag,output] = fminunc(fun,beta_0,options)

fun_nel = @(beta) lhd2(beta,X,y);
disp('MLE Estimation using Nelder Mead method');
neldmead(fun_nel,beta_0)

disp('MLE Estimation using BHHH method');
Tolerance = 10^(-8);
imax = 10000;
beta_mle = BHHH_mle(beta_0,Tolerance,imax,X,y)

%% problem 2
disp('Initial Hessian');
hessian_0 = (((y-exp(X*beta_0))*ones(1,6).*X)'*((y-exp(X*beta_0))*ones(1,6).*X))
eig(hessian_0)
disp('Estimated Hessian');
hessian_mle = (((y-exp(X*beta_mle))*ones(1,6).*X)'*((y-exp(X*beta_mle))*ones(1,6).*X))
eig(hessian_mle)

%%  problem 3 NLS estimation
fun_nls = @(beta) NLLS(beta,X,y);
disp('NLLS Estimation using quasi-newton method without providing gradient');
[beta,fval,exitflag,output] = fminunc(fun_nls,beta_0)


%% problem 4
disp('MLE estimation variance covariance matrix is');
hessian_mle^(-1)


% claculate the variance covariance matrix for NLLS estimator
intermediate1 = 0;
for i = 1:601;
    intermediate1 = intermediate1 + exp(X(i,1:6)*beta)*(-X(i,1:6))'*(exp(X(i,1:6)*beta)*(-X(i,1:6))')';
end
H = 1/601*intermediate1;
disp('NLLS estimation variance covariance matrix is');
H^(-1)
