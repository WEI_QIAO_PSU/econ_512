% This is the log-likelihood function given a set of paprameter value beta, for part B, homework 3 empirical method
% Input is vector of coeficient need to be estimated and data, we require beta being a column vector
% Output is the value of log likelihood function and jacobian, we seek to find the root for this function using various optimization method

function [fval, fjac] = lhd(beta,X,y)
   fval = -(-sum(exp(X*beta)) + sum(y.*(X*beta)) - sum(log(factorial(y))));
   intermediate = zeros(6,1);
       for i = 1:601;
       intermediate = intermediate + exp(X(i,1:6)*beta)*(-X(i,1:6))' + y(i)*X(i,1:6)';
       end
   fjac = intermediate;
end
