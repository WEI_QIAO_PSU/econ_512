% This is the reduced-first order derivative for problem 1, homework 3 empirical method
% Input is a value of theta_1 and data Y=Y1/Y2
% theta_2 will be recovered using theta_2=theta_1/Y1
% Output is the value of first order derivative of the log likelihood function, and the vlaue of Jacobbian. We seek to find the root for this function using Newton's method

function [fval, fjac] = FOC_HW3(theta,Y)
  fval = -log(Y) + log(theta) - psi(theta);
  fjac = 1/theta - trigamma(theta);
end
