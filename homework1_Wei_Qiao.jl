# Your scripts dont run. maybe its due to use of the non standard packages. Next time write the install line so that I can just run script not assuming all your packages are also installed on my computer


# Wei Qiao Homework 1 for Empirical method Class
#Julia Version 0.60
# 5th, Sep, 2017

# Question 1
using Plots
x = [1, 1.5, 3, 4, 5, 7, 9, 10]
y_1 = -2+0.5.*x
plot(x,y_1)
y_2 = -2+0.5*(x.^2)
plot(x,y_2)

#Question2
x_2 = linspace(-10,20,200)
cumsum(x_2)[200]

#Question3
A = [2 4 6;1 7 5;3 12 4]
B = [-2; 3; 10]
C = A'B
D = inv(A'A)B
E = sum(sum(A[i,j]*B[i] for i=1:3) for j=1:3)
F = A[[1,3],[1,2]]
x_3 = inv(A)B

#Question4
G = kron(eye(5),A)

#Question5
A = 5*randn(5,3)+10
B = ones(5,3)
B = reshape(([A[i,j]<10 for j=1:3 for i=1:5]+1-1),5,3).*B

#Question6
using DataFrames
table = readtable("C:\\econ_512_2017\\homework\\datahw1.csv")
Y = table[:,[:5]]
X = table[:,[:3,:4,:6]]
Y = convert(Matrix,Y)
X = convert(Matrix,X)
X = convert(Array{Float64,2},X)
X = [ones(4379,1) X]
Betaa = inv(X'*X)*(X'*Y)
epsilon = Y-X*Betaa
omega = (epsilon*epsilon')
cov = inv(X'*X)*(X'*omega*X)*inv(X'*X)
sd = diag(cov)
T = Betaa./sd
